
#include <thread>
#include <memory>

#include <spdlog.h>
#include "../core/sheep.h"

int main(void)
{

    auto logger = spdlog::stdout_logger_st(SHEEP_LOGGER_NAME);
    if (!logger) {
        std::cerr << "failed to create logger" << std::endl;
        return 1;
    }

    logger->set_level(spdlog::level::debug);

    auto context = std::shared_ptr<woolmark::Context>(new woolmark::Context());
    if (!context) {
        std::cerr << "failed to create context" << std::endl;
        return 1;
    }

    context->set_start_cb([logger](woolmark::Context *context) {
        logger->debug("start thread");
    });
    context->set_stop_cb([logger](woolmark::Context *context) {
        logger->debug("stop thread");
    });

    context->set_update_cb([](woolmark::Context *context) {
        auto logger = spdlog::get(SHEEP_LOGGER_NAME);
        for (auto sheep : *context->sheepFlock) {
            logger->debug("{}, {}", sheep.x, sheep.y);
        }
    });

    /*
    logger->info("width: {0:d}", context->get_width());
    logger->info("height: {0:d}", context->get_height());

    logger->info("fence x: {0:d}", context->frame->fence_x);
    logger->info("fence y: {0:d}", context->frame->fence_y);
    logger->info("fence width: {0:d}", context->frame->fence_width);
    logger->info("fence height: {0:d}", context->frame->fence_height);
    std::cout << *woolmark::get_sheep00_png();
    */

    context->start();

    auto runner = std::unique_ptr<std::thread>(new std::thread([] {
        auto wait_time = std::chrono::milliseconds(5 * 1000);
        std::this_thread::sleep_for(wait_time);
    }));
    if (runner) {
        runner->join();
    } else {
        std::cerr << "failed to create waiting thread, sheep will stop soon" << std::endl;
    }

    context->stop();

    return 0;

}

