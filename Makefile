
CXX	?= c++
CXXFLAGS	=
CXX_FLAGS = -Wall -Wextra -pedantic -std=c++11 -pthread -Ispdlog/include/spdlog \
            -DSPDLOG_DEBUG_ON
CXX_RELEASE_FLAGS = -O3 -march=native
CXX_DEBUG_FLAGS= -g

all: sheep

sheep: spdlog/include/spdlog.h main/main.o core/sheep.o
	$(CXX) -o $@ main/main.o core/sheep.o

spdlog/include/spdlog.h:
	git submodule init spdlog
	git submodule update spdlog

%.o: %.cpp
	$(CXX) -c -o $@ $< $(CXX_FLAGS) $(CXX_RELEASE_FLAGS) $(CXXFLAGS)

run: sheep
	./sheep

valgrind: sheep
	valgrind ./sheep

img/sheep_img.h:
	@echo "#ifndef _Include_sheep_img" > $@
	@echo "#define _Include_sheep_img" >> $@
	@echo >> $@
	@echo "#define SHEEP00_WIDTH `convert img/sheep00.png -print \"%w\n\" /dev/null`" >> $@
	@echo "#define SHEEP00_HEIGHT `convert img/sheep00.png -print \"%h\n\" /dev/null`" >> $@
	@echo "#define SHEEP00_PNG_BASE64 \"`cat img/sheep00.png | base64`\"" >> $@
	@echo >> $@
	@echo "#define SHEEP01_WIDTH `convert img/sheep01.png -print \"%w\n\" /dev/null`" >> $@
	@echo "#define SHEEP01_HEIGHT `convert img/sheep01.png -print \"%h\n\" /dev/null`" >> $@
	@echo "#define SHEEP01_PNG_BASE64 \"`cat img/sheep01.png | base64`\"" >> $@
	@echo >> $@
	@echo "#define FENCE_WIDTH `convert img/fence.png -print \"%w\n\" /dev/null`" >> $@
	@echo "#define FENCE_HEIGHT `convert img/fence.png -print \"%h\n\" /dev/null`" >> $@
	@echo "#define FENCE_PNG_BASE64 \"`cat img/fence.png | base64`\"" >> $@
	@echo >> $@
	@echo "#endif" >> $@

clean:
	- rm -f */*.o sheep

