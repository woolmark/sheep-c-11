# sheep for C++11

sheep for C++11 is an implementation [sheep] for C++11.
That does not include GUI, includes only algrorithm.

# Requirements

 - C++ Compiler supports C++11, ex. gcc4.8.1 and clang3.5

# Dependencies

 - spdlog, C++ header only logging library

# Porting samples

 - Android: https://bitbucket.org/ntakimura/sheep-android-c11
 - iOS: https://bitbucket.org/ntakimura/sheep-ios-c11

# License
[WTFPL]

[sheep]: https://woolmark.bitbucket.org/ "Sheep"
[WTFPL]: http://www.wtfpl.net "WTFPL"

