
#include "sheep.h"

#include <iostream>
#include <thread>
#include <memory>

#include <spdlog.h>

#include "../img/sheep_img.h"

//
// Logger instance
//

static std::shared_ptr<spdlog::logger> logger;

//
// BASE64 Utility functions
//

static std::string base64_decode(std::string const& encoded_string);

//
// Namespace woolmark
//

namespace woolmark {

std::array<int, 3> skyRGB = {{ 150, 150, 255 }};

std::array<int, 3> groundRGB = {{ 100, 255, 100 }};

std::unique_ptr<std::string> sheep00_png(nullptr);

std::unique_ptr<std::string> sheep01_png(nullptr);

std::unique_ptr<std::string> fence_png(nullptr);

static const auto SLEEP_DURATION = std::chrono::milliseconds(100);

static const auto DEFAULT_FRAME_WIDTH = 120;

static const auto DEFAULT_FRAME_HEIGHT = 120;

static const auto SHEEP_MOVE_X_DELTA = 5;

static const auto SHEEP_MOVE_Y_DELTA = 3;

static std::mt19937 random(static_cast<unsigned int>(time(nullptr)));

std::string *get_sheep00_png()
{
    if (!sheep00_png) {
        sheep00_png.reset(new std::string(base64_decode(SHEEP00_PNG_BASE64)));
    }
    return sheep00_png.get();
}

std::string *get_sheep01_png()
{
    if (!sheep01_png) {
        sheep01_png.reset(new std::string(base64_decode(SHEEP01_PNG_BASE64)));
    }
    return sheep01_png.get();
}

std::string *get_fence_png()
{
    if (!fence_png) {
        fence_png.reset(new std::string(base64_decode(FENCE_PNG_BASE64)));
    }
    return fence_png.get();
}

Frame::Frame(int w, int h) : Frame::Frame(w, h, 1)
{
}

Frame::Frame(int w, int h, int scale):
    width(w), height(h)
{
    assert(w >= 0);
    assert(h >= 0);

    fence_width = FENCE_WIDTH * scale;
    fence_height = FENCE_HEIGHT * scale;
    fence_x = (w - fence_width) / 2;
    fence_y = (h - fence_height);

    ground_width = w;
    ground_height = (int)(fence_height * 0.9f);
    ground_x = 0;
    ground_y = height - ground_height;

    sheep_width = SHEEP00_WIDTH * scale;
    sheep_height = SHEEP00_HEIGHT * scale;
}

Sheep::Sheep(Frame *frame) : Sheep::Sheep(frame, 1)
{
}

Sheep::Sheep(Frame *frame, int scale) :
    width(SHEEP00_WIDTH * scale),
    height(SHEEP00_HEIGHT * scale),
    stretchLeg(false),
    jumpState(0)
{
    assert(frame);
    assert(scale > 0);

    auto w = frame->width;
    auto h = frame->height;
    auto fw = frame->fence_width;
    auto fh = frame->fence_height;

    x = w + width;
    y = h - random() % frame->ground_height - height;

    jumpX = -1 * (y - h) * fw / fh + (w - fw) / 2;
}

Context::Context() : Context(DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT)
{
}

Context::Context(int width, int height) :
    frame(std::shared_ptr<Frame>()),
    sheepFlock(std::make_shared<std::vector<Sheep>>()),
    data(nullptr),
    sheep_count(0),
    scale(1),
    running(false),
    gate_widely_open(false),
    start_cb(nullptr),
    stop_cb(nullptr),
    update_cb(nullptr)
{
    assert(width >= 0);
    assert(height >= 0);

    logger = spdlog::get(SHEEP_LOGGER_NAME);

    set_size(width, height);
}

void Context::set_size(int width, int height)
{
    assert(width >= 0);
    assert(height >= 0);

    SPDLOG_DEBUG(logger, "set_size: {},{}", width, height);

    if (width < 0 || height < 0) {
        logger->warn("ignore set_size, because width or height is less than 0, w:{} h:{}", width, height);
        return;
    }

    // Calculate scale with width and height
    int ratioWidth = width / DEFAULT_FRAME_WIDTH;
    int ratioHeight = height / DEFAULT_FRAME_HEIGHT;
    if (ratioWidth > ratioHeight) {
        scale = ratioHeight;
    } else {
        scale = ratioWidth;
    }
    if (scale <= 0) {
        scale = 1;
    }
    SPDLOG_DEBUG(logger, "scale: {}", scale);

    frame.reset(new Frame(width, height, scale));

    sheepFlock->clear();
    if (frame) {
        sheepFlock->push_back(Sheep(frame.get(), scale));
    } else {
        logger->error("fail to create new frame, context stops thread");
        stop();
    }

}

int Context::get_width()
{
    return frame->width;
}

int Context::get_height()
{
    return frame->height;
}

void Context::start()
{

    if (!frame) {
        logger->warn("context has no valid frame");
    } else if (running) {
        logger->debug("context has already run");
    } else {
        logger->info("Start running thread");

        runner = std::unique_ptr<std::thread>(new std::thread([this] {

            if (this->start_cb) {
                this->start_cb(this);
            }

            do {
                if (!this->frame) {
                    logger->warn("context has no valid frame");
                    break;
                }

                this->run();
                std::this_thread::sleep_for(SLEEP_DURATION);
            } while (this->running);

            if (this->stop_cb) {
                this->stop_cb(this);
            }
        }));

        if (!runner) {
            logger->warn("failed to create runner thread");
        } else {
            running = true;
        }

    }

}

void Context::stop()
{
    logger->info("Stop running thread");

    if (running) {
        running = false;

        runner->join();
        runner.release();
    }

}

inline bool Context::is_running()
{
    return running;
}


int Context::get_sheep_count()
{
    return sheep_count;
}

void Context::set_sheep_count(const int count)
{
    sheep_count = count;
}
    
void Context::set_gate_widely_open(bool opened)
{
    gate_widely_open = opened;
}

void Context::run()
{
    SPDLOG_TRACE(logger, "Context#run");

    auto newSheepFlock = std::make_shared<std::vector<Sheep>>();
    if (!newSheepFlock) {
        logger->error("fail to create new sheep flock, context stops thread");
        return;
    }

    if (gate_widely_open) {
        sheepFlock->push_back(Sheep(frame.get(), scale));
    }

    for (auto& sheep : *sheepFlock) {

        sheep.x -= SHEEP_MOVE_X_DELTA * scale;

        if (sheep.x < sheep.jumpX && sheep.jumpState < 3) {
            sheep.y -= SHEEP_MOVE_Y_DELTA * scale;
            sheep.stretchLeg = true;

            sheep.jumpState++;
        } else if (sheep.x < sheep.jumpX && sheep.jumpState < 6) {
            if (sheep.jumpState == 3) {
                sheep_count++;
            }

            sheep.y += SHEEP_MOVE_Y_DELTA * scale;
            sheep.stretchLeg = true;

            sheep.jumpState++;
        } else {
            sheep.stretchLeg = !sheep.stretchLeg;
        }

        if (sheep.x > -1 * sheep.width) {
            newSheepFlock->push_back(sheep);
        }

    }

    sheepFlock = newSheepFlock;
    if (sheepFlock->size() == 0) {
        sheepFlock->push_back(Sheep(frame.get(), scale));
    }

    if (update_cb) {
        update_cb(this);
    }

}

void Context::set_start_cb(std::function<void(Context *)> cb)
{
    start_cb = cb;
}

void Context::set_stop_cb(std::function<void(Context *)> cb)
{
    stop_cb = cb;
}

void Context::set_update_cb(std::function<void(Context *)> cb)
{
    update_cb = cb;
}

}

static const std::string base64_chars =
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";

static inline bool is_base64(unsigned char c) {
  return (isalnum(c) || (c == '+') || (c == '/'));
}

static std::string base64_decode(std::string const& encoded_string) {
  int in_len = encoded_string.size();
  int i = 0;
  int j = 0;
  int in_ = 0;
  unsigned char char_array_4[4], char_array_3[3];
  std::string ret;

  while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
    char_array_4[i++] = encoded_string[in_]; in_++;
    if (i ==4) {
      for (i = 0; i <4; i++)
        char_array_4[i] = base64_chars.find(char_array_4[i]);

      char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
      char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
      char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

      for (i = 0; (i < 3); i++)
        ret += char_array_3[i];
      i = 0;
    }
  }

  if (i) {
    for (j = i; j <4; j++)
      char_array_4[j] = 0;

    for (j = 0; j <4; j++)
      char_array_4[j] = base64_chars.find(char_array_4[j]);

    char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
    char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
    char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

    for (j = 0; (j < i - 1); j++) ret += char_array_3[j];
  }

  return ret;
}



