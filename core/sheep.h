
#pragma once
#ifndef _Included_sheep
#define _Included_sheep

#include <array>
#include <thread>
#include <random>

#define SHEEP_LOGGER_NAME "sheep"

namespace woolmark {

extern std::array<int, 3> skyRGB;

extern std::array<int, 3> groundRGB;

std::string *get_sheep00_png();

std::string *get_sheep01_png();

std::string *get_fence_png();

struct Frame {

public:

    int width;

    int height;
    
    int ground_x;
    
    int ground_y;

    int ground_height;

    int ground_width;

    int fence_x;

    int fence_y;

    int fence_width;

    int fence_height;

    int sheep_width;

    int sheep_height;

    Frame(int, int);

    Frame(int, int, int);

};

struct Sheep {

public:

    int x;

    int y;

    int width;

    int height;

    bool stretchLeg;

    int jumpX;

    int jumpState;

    Sheep(Frame *);

    Sheep(Frame *, int);

};


class Context {

public:

    std::shared_ptr<Frame> frame;

    std::shared_ptr<std::vector<Sheep>> sheepFlock;

    void *data;

    Context();

    Context(int, int);

    void set_size(int, int);

    int get_height();

    int get_width();

    void start();

    void stop();

    void run();

    bool is_running();

    int get_sheep_count();

    void set_sheep_count(const int);
    
    void set_gate_widely_open(bool opened);

    void set_start_cb(std::function<void(Context *)>);

    void set_stop_cb(std::function<void(Context *)>);

    void set_update_cb(std::function<void(Context *)>);

protected:

    int sheep_count;

    int scale;

    bool running;

    bool gate_widely_open;

    std::unique_ptr<std::thread> runner;

    std::function<void(Context *)> start_cb;

    std::function<void(Context *)> stop_cb;

    std::function<void(Context *)> update_cb;

};

}

#endif


